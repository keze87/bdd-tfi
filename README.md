# BdD-TFI



## Dependencias

* Python ¿3.9?
* mysql-connector
* PyQt5

## Opcionales

* pylama
* yapf
* cweijan.vscode-mysql-client2

## Links

https://realpython.com/python-pyqt-database/

https://realpython.com/python-mysql/
