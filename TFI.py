import sys

import mysql.connector as mysql
from mysql.connector import Error

from PyQt5.QtWidgets import (QApplication, QMainWindow, QMessageBox, QLabel,
                             QTableWidget, QTableWidgetItem, QWidget,
                             QVBoxLayout, QFormLayout, QLineEdit, QDialog,
                             QDialogButtonBox)
from PyQt5.QtGui import QFont


class Vista(QMainWindow):
    def __init__(self,
                 enunciado: str,
                 encabezado: list,
                 consulta: list,
                 parent=None):

        super().__init__(parent)

        self.setWindowTitle("TFI")

        widget = QWidget()
        vbox = QVBoxLayout()
        tabla = QTableWidget()
        texto = QLabel(enunciado)

        texto.setFont(QFont('Victor Mono', 30))
        texto.setWordWrap(True)

        vbox.addWidget(texto)

        # Cargar la vista en la tabla
        tabla.setColumnCount(len(encabezado))
        tabla.setHorizontalHeaderLabels(encabezado)

        for tupla in consulta:
            rows = tabla.rowCount()
            tabla.setRowCount(rows + 1)

            for atributo, valor in enumerate(tupla):
                if valor:
                    tabla.setItem(rows, atributo, QTableWidgetItem(str(valor)))
                else:
                    tabla.setItem(rows, atributo, QTableWidgetItem(' '))

        tabla.setFont(QFont('Victor Mono', 25))
        tabla.resizeColumnsToContents()
        tabla.resizeRowsToContents()

        vbox.addWidget(tabla)

        widget.setLayout(vbox)

        self.setCentralWidget(widget)


def imprimir_error(err) -> None:
    print(f"Error: '{err}'")

    QMessageBox.critical(
        None,
        "Error!",
        "Database Error: \n%s" % err,
    )


def crear_coneccion_mysql(host_name, user_name, basededatos):
    connection = None
    try:
        connection = mysql.connect(host=host_name,
                                   user=user_name,
                                   passwd='pass',
                                   database=basededatos)

        print("MySQL Database connection successful")
    except Error as err:
        imprimir_error(err)

        sys.exit(1)

    return connection


def read_query(connection, query) -> list:
    cursor = connection.cursor()

    try:
        cursor.execute(query)
        result = cursor.fetchall()

        print(f"\nResultado de \"{query}\"")
        print(result)

        return result
    except Error as err:
        imprimir_error(err)

        return []


def llamar_procedimiento(connection, procedimiento: str, query) -> list:
    cursor = connection.cursor()
    retorno: list = []

    try:
        cursor.callproc(procedimiento, args=([query]))

        for result in cursor.stored_results():
            retorno = result.fetchall()

        print(f"\nResultado de \"{query}\"")
        print(retorno)

    except Error as err:
        imprimir_error(err)

    return retorno


def crear_ventana(aplicacion,
                  connection,
                  enunciado: str,
                  encabezado: list,
                  query: str):

    consulta = read_query(connection, query)

    if consulta:
        ventana = Vista(enunciado,
                        encabezado,
                        consulta)

        ventana.show()

        return aplicacion.exec_()

    else:
        return 0


class InputDialog(QDialog):
    def __init__(self, connection, parent=None):
        super().__init__(parent)

        self.first = QLineEdit(self)
        buttonBox = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel, self)

        layout = QFormLayout(self)
        layout.addRow("Elegir DNI", self.first)
        layout.addWidget(buttonBox)

        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

    def getInputs(self):
        return (self.first.text())


def elegir_dni(connection) -> int:
    dialog = InputDialog(connection)

    if dialog.exec():
        return dialog.getInputs()

    return 0


def crear_ventana_procedimiento(aplicacion,
                                connection,
                                enunciado: str,
                                encabezado: list,
                                procedimiento: str):

    numero_dni: int = elegir_dni(connection)

    consulta = llamar_procedimiento(connection, procedimiento, numero_dni)

    if consulta:
        ventana = Vista(enunciado,
                        encabezado,
                        consulta)

        ventana.show()

        return aplicacion.exec_()

    else:
        return 0


app = QApplication(sys.argv)

coneccion = crear_coneccion_mysql("localhost", "keze", "TFI")

crear_ventana(app,
              coneccion,
              "1) Datos del paciente, incluyendo enfermedades hereditarias o trastornos o discapacidad previa a la consulta.",
              ["Apellido", "Nombre", "DNI", "Comorbilidades", "Enfermedades"],
              "SELECT * FROM vista_consulta_1;")

crear_ventana_procedimiento(app,
                            coneccion,
                            "2) Historial clínico del paciente completo, información del enfermedades que tuvo, fecha, tratamiento, nombre del profesional que lo atendió y lugar en el que fue atendido.",
                            ["Enfermedad", "Fecha de Diagnostico",
                             "Nombre de Profesional", "Tratamientos",
                             "Medicamentos", "Nombre de Nosocomio"],
                            "procedimiento_consulta_2")

crear_ventana(app,
              coneccion,
              "3) Listado de pacientes que no recibieron el alta de internación.",
              ["Apellido", "Nombre", "DNI", "Alta"],
              "SELECT * FROM vista_consulta_3;")

crear_ventana(app,
              coneccion,
              "4) Listar los pacientes que se encuentran internados por COVID en los nosocomios, cual es el nro de habitación que se encuentran, estado del mismo y catalogado por nosocomio.",
              ["Apellido", "Nombre", "Habitación",
                  "Estado SEIRD", "Nombre Nosocomio"],
              "SELECT * FROM vista_consulta_4;")

crear_ventana(app,
              coneccion,
              "5) Listar los profesionales de la salud especializados en infectología e incluyendo en que nosocomio trabajan.",
              ["Apellido", "Nombre", "DNI",
               "Especialidad", "Nombre Nosocomio"],
              "SELECT * FROM vista_consulta_5;")
